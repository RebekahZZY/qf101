% QF101
% NUSInvest
% October 20, 2018

# ![](img/QF101Poster.jpeg){width=50%}

# Before we begin...

## Disclaimer

NUS Invest has partnered with QuantInsti to present the
QF101 workshop. NUS Invest will not be liable for students
who pursue any further engagement with QuantInsti beyond
the scope of QF101, and are to do so at their own risk and
capacity.

Also, what is being shared here is purely for educational
purposes. Any capital loss or gain as a result of applying
the techniques learned in this workshop is not the
responsibility of the speaker(s) involved.

## Resources

Slides and other resources available at
[https://gitlab.com/NUSInvest/qf101](https://gitlab.com/NUSInvest/qf101)  
Questions? Send an issue tracker!

# Introduction to Algo Trading

##

My personal experience in being introduced to the world of
retail algorithmic Forex trading

# Personal Introduction

##

- Year 3 QF Student
- Almost went into Chemistry
- Started with MQL4
- Now trading on Python

# Trading

## What is Trading?

- Buy low, sell high
- Sell high, buy low
- Make money by risking money

## Trading vs Investing

- Short term
- SLs & TPs
- Leverage

## Assets to Trade

- Equities
- Indices
- ETFs
- Forex
- Futures
- Options

# Algorithmic Trading

## Flow

![](img/algoTradeFlow.png)

# Extract, Transform, Load

## Extracting Data

- OHLCV data from brokers
- Raw data (Twitter, annual reports, FOMC meeting minutes, etc.)

## Transforming Data

- Indicators
- Signals

## Loading Data

Trading Decisions:  

- Close Trades/Orders
- Modify Trades/Orders
- Open Trades/Orders

# Results Analysis

##

- Returns
- Drawdowns
- Benchmarks
- Thresholds (Variance)

# Optimisation

## Motive

- Higher Returns
- Lower Risk
- Robust Parameters

## Methods

- Monte Carlo simulations
  + Parameter Permutation
  + Delta Reshuffling (Data)
- Genetic Algorithm
- In-out Sampling

## Overfitting

# Example

## MA Crossover Strategy

![](img/movingAverage.png)

## MA Crossover Strategy

- OHLCV: EURUSD
- Parameters:
  + fastMA = 20
  + slowMA = 50
- Indicators:
  + MACross = SMA(fastMA) - SMA(slowMA)

## MA Crossover Strategy

Conditions

| | Buy | Sell |
| --: | :-: | :-: |
| Open | <span style="font-size: 30px">MACross.shift(1) > 0 \n MACross.shift(2) < 0</span> | <span style="font-size: 30px">MACross.shift(1) < 0 \n MACross.shift(2) > 0</span> |
| Close | <span style="font-size: 30px">MACross.shift(1) < 0 \n MACross.shift(2) > 0</span> | <span style="font-size: 30px">MACross.shift(1) > 0 \n MACross.shift(2) < 0</span> |

# Summary

## 

![](img/algoTradeFlow.png)

# End
